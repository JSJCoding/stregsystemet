﻿using System;
using System.IO;
using System.Linq;
using System.Collections.Generic;
using System.Text.RegularExpressions;

namespace ExamAssigment
{
    public class Stregsystem : IStregsystem
    {
        public List<User> users = new List<User>();
        public List<Product> products = new List<Product>();
        public List<Transaction> transactions = new List<Transaction>();
        public IEnumerable<Product> ActiveProducts => products.Where(product => product.active == 1);
        public event UserBalanceNotification UserBalanceWarning;
        private int transactionID { get; set; }

        public Stregsystem()
        {
            foreach (string line in File.ReadLines("products.csv"))
            {
                if (line.StartsWith("id")) continue;
                string[] values = Regex.Replace(line, @"<.*?>", "").Split(';');
                int id = int.Parse(values[0]);
                decimal price = decimal.Parse(values[2]);
                string name = values[1].Trim('"');
                int active = int.Parse(values[3]);

                products.Add(new Product(id, name, price, active));
            }

            foreach (string line in File.ReadLines("users.csv"))
            {
                if (line.StartsWith("id")) continue;
                string[] values = line.Split(',');
                int ID = int.Parse(values[0]);
                decimal balance = decimal.Parse(values[4]);

                if (!Regex.IsMatch(values[3], @"^[\w\d_]+$"))
                    throw new ErrorInDatabaseException("users", "username", line);

                if (!Regex.IsMatch(values[5], @"[\w\d-_\.]+@([\w\d\.])+[\w]"))
                    throw new ErrorInDatabaseException("users", "email", line);

                users.Add(new User(ID, values[1], values[2], values[3], balance, values[5]));
            }
            users.Sort();
            transactionID = File.ReadLines("transactions.csv").Count() - 1;
        }

        public InsertCashTransaction AddCreditsToAccount(User user, int amount)
        {
            InsertCashTransaction transaction = new InsertCashTransaction(transactionID++, user, amount);
            transaction.Execute();
            transactions.Add(transaction);
            return transaction;
        }

        public BuyTransaction BuyProducts(User user, Product product, int amount = 1)
        {
            BuyTransaction transaction = null;
            if (product.active != 1)
                throw new ProductStatusException(product);

            if (user.balance < (product.price * amount))
                throw new InsufficientCashException(user, product, amount);

            for (int i = 0; i < amount; i++)
            {
                transaction = new BuyTransaction(transactionID++, user, product);
                transaction.Execute();
                transactions.Add(transaction);
            }

            return transaction;
        }

        public Product GetProductByID(int id)
        {
            Product result = products.Find(product => product.id == id);

            if (result == null)
                throw new ProductNotFoundException(id);

            return result;
        }

        public IEnumerable<Transaction> GetTransactions(User user, int amount)
        {
            if (amount > 10) { amount = 10; }
            if (amount < 0) { amount = 0; }
            IEnumerable<Transaction> result = transactions.Where(transaction => transaction.user == user).Take(amount);

            return result;
        }

        public IEnumerable<User> GetUsers(Func<User, bool> predicate)
        {
            IEnumerable<User> result = users.Where(predicate);
            return result;
        }

        public User GetUserByUsername(string username)
        {
            User result = users.Find(user => user.username == username);

            if (result == null)
                throw new UserNotFoundException(username);

            return result;
        }

        public int GetTransactionID()
        {
            int result = transactionID;
            transactionID += 1;
            return result;
        }

        public void OnUserBalanceNotification(decimal balance)
        {
            if (balance < 5000)
            {
                UserBalanceWarning(this, balance);
            }
        }
    }
}