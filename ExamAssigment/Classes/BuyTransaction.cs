﻿using System.IO;

namespace ExamAssigment
{
	public class BuyTransaction : Transaction
	{
		public Product product { get; set; }

		public BuyTransaction(int id, User user, Product product) : base(id, user, -product.price)
		{
			this.product = product;
			File.AppendAllLines("transactions.csv", new string[] { $"{ToString()}" });
		}
	}
}