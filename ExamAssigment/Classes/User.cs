﻿using System;

namespace ExamAssigment
{
    public class User : IComparable<User>
    {
        public int id { get; set; }
        public string firstname { get; set; }
        public string lastname { get; set; }
        public string username { get; set; }
        public string email { get; set; }
        public decimal balance { get; set; }

        public User(int id, string firstname, string lastname, string username, decimal balance, string email)
        {
            this.id = id;
            this.firstname = firstname;
            this.lastname = lastname;
            this.username = username;
            this.balance = balance;
            this.email = email;
        }

        public User(string firstname, string lastname, string username, string email)
        {
            this.firstname = firstname;
            this.lastname = lastname;
            this.username = username;
            this.balance = 0;
            this.email = email;
        }

        public override bool Equals(object obj)
        {
            if (obj == null || !(obj is User))
                return false;
            else
                return id == ((User)obj).id;
        }

        public override int GetHashCode()
        {
            return id;
        }

        public override string ToString()
        {
            return $"{id},{firstname},{lastname},{username},{balance},{email}";
        }

        public int CompareTo(User obj)
        {
            return id.CompareTo(obj.id);
        }
    }
}