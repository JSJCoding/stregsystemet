﻿namespace ExamAssigment
{
    public class Product
    {
		public int id { get; set; }
		public string name { get; set; }
		public decimal price { get; set; }
		public int active { get; set; }
		public bool canBeBoughtOnCredit { get; set; }

		public Product(int id, string name, decimal price, int active)
		{
			this.id = id;
			this.name = name;
			this.price = price;
			this.active = active;
		}

		public override string ToString()
		{
			return $"{id} {name} {price}";
		}
	}
}