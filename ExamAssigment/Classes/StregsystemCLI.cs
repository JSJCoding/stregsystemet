﻿using System;
using System.Linq;
using System.Collections.Generic;

namespace ExamAssigment
{
    public class StregsystemCLI : IStregsystemUI
    {
		public bool running { get; set; }
		public List<Action> messages { get; set; }
		private int _tableWidth = 67;
		private int cursorPosY = 1;
		private string tableHead = String.Format("|  {0, -6}  |  {1,-40}  |  {2,-5}  |", "ID", "NAME", "PRICE");
		private string tableBody;

		IStregsystem stregsystem { get; set; }
		public event StregsystemEvent CommandEntered;


		public StregsystemCLI(IStregsystem stregsystem)
		{
			this.stregsystem = stregsystem;
			this.messages = new List<Action>();
		}

		public void Start()
		{
			running = true;
		}

		public void Close()
		{
			running = false;
		}

		public string GetUserCommand()
		{
			string result = Console.ReadLine().Trim();

			return result;
		}

		public void Display()
		{
			if (messages.Count != 0)
			{
				foreach (Action message in messages)
				{
					Console.SetCursorPosition(70, cursorPosY++);
					message.Invoke();
				}
				messages.Clear();
				cursorPosY = 1;
			}

			Console.SetCursorPosition(0, 0);
			Console.WriteLine(new string('-', _tableWidth));
			Console.WriteLine(tableHead);
			Console.WriteLine(new string('-', _tableWidth));

			foreach (Product product in stregsystem.ActiveProducts)
			{
				tableBody = String.Format("|  {0, -6}  |  {1,-40}  |  {2,-5}  |", product.id, product.name, product.price);
				Console.WriteLine(tableBody);
			}

			Console.WriteLine(new string('-', _tableWidth));
			Console.Write("Type your command: ");
		}

		public void DisplayUserNotFound(string username)
		{
			Console.WriteLine($"User {username} not found!");
		}

		public void DisplayProductNotFound(string product)
		{
			Console.WriteLine($"No product of name {product} was not found.");
		}

		public void DisplayUserInfo(User user)
		{
			Console.WriteLine($"{user.firstname} {user.lastname} {user.balance}");
		}

		public void DisplayTooManyArgumentsError(string command)
		{
			Console.WriteLine($"There are too many arguments in the command: {command}.");
		}

		public void DisplayUserBuysProduct(BuyTransaction transaction)
		{
			Console.WriteLine($"{transaction.user.username} has bought one item of \"{transaction.product.name}\".");
		}

		public void DisplayUserBuysProduct(int count, BuyTransaction transaction)
		{
			Console.WriteLine($"{transaction.user.username} has bought {count} items of {transaction.product.name}.");
		}

		public void DisplayUserTransactionHistory(IEnumerable<Transaction> transactions)
		{
			if (transactions.Count() != 0)
			{
				foreach (Transaction transaction in transactions)
				{
					Console.WriteLine(transaction);
					Console.SetCursorPosition(70, cursorPosY++);
				}
			}
			else
			{
				Console.WriteLine("You currently have no transactions in the system!");
			}
		}

		public void DisplayInsufficientCash(User user, Product product)
		{
			Console.WriteLine($"You do not have sufficient balance to buy this item. Your current balance is {user.balance} and the price of the item is {product.price}.");
		}

		public void DisplayGeneralError(string errorString)
		{
			Console.WriteLine(errorString);
		}

		public void OnCommandEntered(string command)
		{
			if (CommandEntered != null)
			{
				CommandEntered(this, command);
			}
		}

		public void DisplayCommand(object source, string arg)
		{
			Console.WriteLine($"Command Entered: {arg}");
		}

		public void DisplayUserBalanceWarning(object source, decimal arg)
		{
			Console.WriteLine($"Your balance is currently below 5000! Your balance is {arg}.");
		}
	}
}