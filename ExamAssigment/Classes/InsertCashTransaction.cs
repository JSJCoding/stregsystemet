﻿using System.IO;

namespace ExamAssigment
{
    public class InsertCashTransaction : Transaction
    {
        public InsertCashTransaction(int id, User user, decimal amount) : base(id, user, amount)
        {
            File.AppendAllLines("transactions.csv", new string[] { $"{ToString()}" });
        }
    }
}