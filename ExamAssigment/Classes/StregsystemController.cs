﻿using System;
using System.Linq;
using System.Reflection;
using System.Collections.Generic;

namespace ExamAssigment
{
    public class StregsystemController
    {
		public IStregsystemUI ui { get; set; }
		public IStregsystem stregsystem { get; set; }

		private Dictionary<int, Action<string[]>> _userCommands;
		private Dictionary<string, Action<string[]>> _adminCommands;

		public StregsystemController(IStregsystemUI ui, IStregsystem stregsystem)
		{
			this.ui = ui;
			this.stregsystem = stregsystem;
			this._userCommands = new Dictionary<int, Action<string[]>>()
			{
				{ 1, (args) =>
					{
						User user = stregsystem.GetUserByUsername(args[0]);
						IEnumerable<Transaction> transactions = stregsystem.GetTransactions(user, 10);
						ui.messages.Add(() => { ui.DisplayUserInfo(user);});
						ui.messages.Add(() => { ui.DisplayUserTransactionHistory(transactions); });
						ui.messages.Add(() => { stregsystem.OnUserBalanceNotification(user.balance); });
					}
				},
				{ 2, (args) =>
					{
						User user = stregsystem.GetUserByUsername( args[0] );
						Product product = stregsystem.GetProductByID( int.Parse(args[1]) );
						BuyTransaction transaction = stregsystem.BuyProducts(user, product);
						ui.messages.Add(() => { ui.DisplayUserBuysProduct(transaction); });
						ui.messages.Add(() => { stregsystem.OnUserBalanceNotification(user.balance); });
					}
				},
				{ 3, (args) =>
					{
						int amount = int.Parse(args[2]);
						User user = stregsystem.GetUserByUsername( args[0] );
						Product product = stregsystem.GetProductByID( int.Parse(args[1]) );
						BuyTransaction transaction = null;
						transaction = stregsystem.BuyProducts(user, product, amount);
						ui.messages.Add(() => { ui.DisplayUserBuysProduct(amount, transaction); });
						ui.messages.Add(() => { stregsystem.OnUserBalanceNotification(user.balance); });
					}
				}
			};

			this._adminCommands = new Dictionary<string, Action<string[]>>()
			{
				{ ":q",             (args) => { ui.Close(); }},
				{ ":quit",          (args) => { ui.Close(); }},
				{ ":activate",      (args) => { stregsystem.GetProductByID( int.Parse(args[1]) ).active = 1; }},
				{ ":deactivate",    (args) => { stregsystem.GetProductByID( int.Parse(args[1]) ).active = 0; }},
				{ ":crediton",      (args) => { stregsystem.GetProductByID( int.Parse(args[1]) ).canBeBoughtOnCredit = true; }},
				{ ":creditoff",     (args) => { stregsystem.GetProductByID( int.Parse(args[1]) ).canBeBoughtOnCredit = false; }},
				{ ":addcredits",    (args) => { stregsystem.AddCreditsToAccount(stregsystem.GetUserByUsername( args[1]), int.Parse(args[2])); }},
				{ ":getusers",      (args) => { stregsystem.GetUsers((user) => user.id < 5); }}
			};
		}

		public void ParseCommand(string userInput)
		{
			ui.CommandEntered += ui.DisplayCommand;
			stregsystem.UserBalanceWarning += ui.DisplayUserBalanceWarning;
			ui.messages.Add(() => { ui.OnCommandEntered(userInput); });

			try
			{
				string[] args = userInput.Split(" ");
				switch (userInput[0])
				{
					case ':':
						if (_adminCommands.ContainsKey(args[0]) != true)
							throw new AdminCommandNotFoundException(args[0]);
						_adminCommands[args[0]].Invoke(args); break;
					default:
						if (_userCommands.Count() < args.Length)
							throw new TooManyArgumentsException(args.Length);
						_userCommands[args.Length].DynamicInvoke((object)args);
						break;
				}
			}
			catch (AdminCommandNotFoundException e)
			{
				ui.messages.Add(() => { Console.WriteLine(e.Message); });
			}
            catch (TooManyArgumentsException e)
			{
				ui.messages.Add(() => { Console.WriteLine(e.Message); });
			}
			catch (ProductNotFoundException e)
			{
				ui.messages.Add(() => { Console.WriteLine(e.Message); });
			}
			catch (ProductStatusException e)
			{
				ui.messages.Add(() => { Console.WriteLine(e.Message); });
			}
			catch (InsufficientCashException e)
			{
				ui.messages.Add(() => { Console.WriteLine(e.Message); });
			}
			catch (TargetInvocationException e) // Catches UserNotFoundException
			{
				ui.messages.Add(() => { Console.WriteLine(e.InnerException.Message); });
			}
		}
	}
}