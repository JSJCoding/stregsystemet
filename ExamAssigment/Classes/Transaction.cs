﻿using System;

namespace ExamAssigment
{
	public class Transaction
	{
		public int id { get; set; }
		public User user { get; set; }
		public DateTime date { get; set; }
		public decimal amount { get; set; }

		public Transaction(int id, User user, decimal amount)
		{
			this.id = id;
			this.user = user;
			this.amount = amount;
			date = DateTime.Now;
		}

		public override string ToString()
		{
			return $"{id},{this.GetType().Name},{amount},{date},{user.id},{user.username}";
		}

		public void Execute()
		{
			user.balance += amount;
		}
	}
}