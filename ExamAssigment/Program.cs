﻿using System;

namespace ExamAssigment
{
    class Program
    {
        static void Main(string[] args)
        {
            IStregsystem stregsystem = new Stregsystem();
            IStregsystemUI ui = new StregsystemCLI(stregsystem);
            StregsystemController controller = new StregsystemController(ui, stregsystem);

            ui.Start();
            do
            {
                try
                {
                    Console.Clear();
                    ui.Display();
                    controller.ParseCommand(ui.GetUserCommand());
                }
                catch (IndexOutOfRangeException e)
                {
                    ui.messages.Add(() => { Console.WriteLine("[No Command Entered]"); });
                }
            } while (ui.running);
        }
    }
}
