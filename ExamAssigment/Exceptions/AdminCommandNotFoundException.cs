﻿using System;

namespace ExamAssigment
{
    internal class AdminCommandNotFoundException : Exception
    {
        public AdminCommandNotFoundException(string command) : base($"The admin command {command} was not found")
        {
        }
    }
}