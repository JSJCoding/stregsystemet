﻿using System;

namespace ExamAssigment
{
    internal class ProductNotFoundException : Exception
    {
        public ProductNotFoundException(int id) : base($"No product with the give id '{id}' was found")
        {
        }
    }
}