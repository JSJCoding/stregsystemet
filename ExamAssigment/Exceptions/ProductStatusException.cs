﻿using System;

namespace ExamAssigment
{
    internal class ProductStatusException : Exception
    {
        public ProductStatusException(Product product) : base($"{product.name} is currently inactive [{product.active}].")
        {
        }
    }
}