﻿using System;

namespace ExamAssigment
{
    internal class ErrorInDatabaseException : Exception
    {
        public ErrorInDatabaseException(string database, string field, string line) : base($"Invaild {field} in the database '{database}' on line: {line}")
        {
        }
    }
}