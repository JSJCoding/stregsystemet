﻿using System;

namespace ExamAssigment
{
    internal class InsufficientCashException : Exception
    {
        public InsufficientCashException(User user, Product product, int amount) : base($"{user.username} current balance {user.balance} is insuffience! Your require {(product.price * amount) - user.balance}.")
        {
        }
    }
}