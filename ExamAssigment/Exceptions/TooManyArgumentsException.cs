﻿using System;

namespace ExamAssigment
{
    internal class TooManyArgumentsException : Exception
    {
        public TooManyArgumentsException(int argsLength) : base($"{argsLength} Is too many arguments!!")
        {
        }
    }
}