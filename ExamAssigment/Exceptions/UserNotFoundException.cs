﻿using System;

namespace ExamAssigment
{
    internal class UserNotFoundException : Exception
    {
        public UserNotFoundException(string username) : base($"The user with username '{username}' was not found")
        {
        }
    }
}