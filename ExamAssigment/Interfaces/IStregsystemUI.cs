﻿using System;
using System.Collections.Generic;

namespace ExamAssigment
{
	public delegate void StregsystemEvent(object source, string arg);
	public interface IStregsystemUI
    {
		List<Action> messages { get; set; }
		bool running { get; set; }
		event StregsystemEvent CommandEntered;
		void Start();
		void Close();
		void Display();
		string GetUserCommand();
		void DisplayUserNotFound(string username);
		void DisplayProductNotFound(string product);
		void DisplayUserInfo(User user);
		void DisplayTooManyArgumentsError(string command);
		void DisplayUserBuysProduct(BuyTransaction transaction);
		void DisplayUserBuysProduct(int count, BuyTransaction transaction);
		void DisplayUserTransactionHistory(IEnumerable<Transaction> transactions);
		void DisplayInsufficientCash(User user, Product product);
		void DisplayGeneralError(string errorString);

		void OnCommandEntered(string command);
		void DisplayUserBalanceWarning(object source, decimal arg);
		void DisplayCommand(object source, string arg);
	}
}