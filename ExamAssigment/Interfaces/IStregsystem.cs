﻿using System;
using System.Collections.Generic;

namespace ExamAssigment
{
    public delegate void UserBalanceNotification(object source, decimal arg);
    public interface IStregsystem
    {
        IEnumerable<Product> ActiveProducts { get; }
        InsertCashTransaction AddCreditsToAccount(User user, int amount);
        BuyTransaction BuyProducts(User user, Product product, int amount = 1);
        Product GetProductByID(int id);
        IEnumerable<Transaction> GetTransactions(User user, int amount);
        IEnumerable<User> GetUsers(Func<User, bool> predicate);
        User GetUserByUsername(string username);
        event UserBalanceNotification UserBalanceWarning;
        void OnUserBalanceNotification(decimal balance);
    }
}