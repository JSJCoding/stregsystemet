﻿using ExamAssigment;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ExamAssigment.Tests
{
    [TestClass()]
    public class StregsystemTests
    {
        [TestMethod()]
        public void AddCreditsToAccountTest()
        {
            Stregsystem stregsystem = new Stregsystem();
            User user = new User("firstnameTest", "lastnameTest", "usetnameTest", "emailTest");
            int amount = 10000;
            decimal expected = 10000;

            stregsystem.AddCreditsToAccount(user, amount);

            decimal actual = user.balance;
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void BuyProductsTest()
        {
            Stregsystem stregsystem = new Stregsystem();
            User user = new User("firstnameTest", "lastnameTest", "usetnameTest", "emailTest") { balance = 1200 };
            Product product = new Product(0, "productnameTest", 1200, 1);
            decimal expectedUserBalance = 0;

            stregsystem.BuyProducts(user, product);

            decimal actualUserBalance = user.balance;
            Assert.AreEqual(expectedUserBalance, actualUserBalance);
        }

        [TestMethod()]
        public void GetProductByIDTest()
        {
            Stregsystem stregsystem = new Stregsystem();
            string expected = "16 Cocio 1600";

            Product product = stregsystem.GetProductByID(16);

            string actual = product.ToString();
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void GetUsersTest()
        {
            Stregsystem stregsystem = new Stregsystem();
            Func<User, bool> predicate = user => user.id == 12;
            string expected = stregsystem.GetUserByUsername("rusling").ToString();

            IEnumerable<User> users = stregsystem.GetUsers(predicate);

            string actual = users.First().ToString();

            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void GetUserByUsernameTest()
        {
            Stregsystem stregsystem = new Stregsystem();
            string expected = "12,Jonas,Johansen,rusling,0,rusling@sample.stregsystem.dk";

            User user = stregsystem.GetUserByUsername("rusling");

            string actual = user.ToString();
            Assert.AreEqual(expected, actual);
        }
    }
}