## Project status
The project is, as fare as I am concerned, complete following the end of the course in object-oriented programming.

## Description
[Stregsystemet](https://github.com/f-klubben/stregsystemet) is a system used by [F-klubben](https://fklub.dk) a pastime association at Aalborg University. The goal of this project is to emulate the basic functionality and use case of the system, that is the ability to:
- to allow users to see their information,
- to allow users to buy products,
- to manage products, and
- to give users credits they can use to purchase products. 

## Visuals
![Image providing an example of how the system looks and is used](exampleImage.PNG)

## Usage
There are two types of available commands: "user commands" and "admin commands". Note that the system does not consider account security level and all commands are available in the same terminal instance. (It was not part of the exercise) 

### User Commands
Users have three available commands depending on the number of arguments they provide:

| Command     | Arguments                 | Description                                                     |
|-------------|---------------------------|-----------------------------------------------------------------|
| 1 argument  | username                  | Displays user information incl. credits and transaction history |
| 2 arguments | username productID        | Creates a buy user transaction of a given product               |
| 3 arguments | username productID amount | Creates multiple buy user transactions of a given product       |

### Admin Commands
| Command     | Arguments        | Description                                       |
|-------------|------------------|---------------------------------------------------|
| :q or :quit | none             | Exits the application                             |
| :activate   | productID        | Activates a product                               |
| :deactivate | productID        | Deactivates a product                             |
| :crediton   | productID        | Product can only be bought on credits             |
| :credioff   | productID        | Product can be bought without credits             |
| :addcredits | username credits | Gives a specified amount of credits to an account |

Remember to add the additional ":" to denote that it is an admin command.

